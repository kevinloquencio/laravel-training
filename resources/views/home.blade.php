@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">

                    <h3>{{ __("Welcome {$user->name}!") }}</h3>


                    <div class="p-2"></div>
                    <div class="alert alert-primary" role="alert">
                        Employee registered <h3>{{ $employee_count }}</h3>
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        Companies Registered <h3>{{ $company_count }}</h3>
                    </div>
                    <div class="alert alert-success" role="alert">
                        Users Registered <h3>{{ $user_count }}</h3>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
