<select name="limit" id="limit" class="form-control" title="Page limit">
    <option value=""> -- Page Limit -- </option>
    <option value="10" {{ $limit == "10" ? 'selected': '' }}>10</option>
    <option value="30" {{ $limit == "30" ? 'selected': '' }}>30</option>
    <option value="50" {{ $limit == "50" ? 'selected': '' }}>50</option>
    <option value="80" {{ $limit == "80" ? 'selected': '' }}>80</option>
    <option value="100" {{ $limit == "100" ? 'selected': '' }}>100</option>
</select>