@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3>We could not find the page you are looking for!</h3>
                    <a href="/">Back to home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
