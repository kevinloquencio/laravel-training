@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <h3>The specific request is not supported!</h3>
                    <a href="{{ url()->previous() }}">Go Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
