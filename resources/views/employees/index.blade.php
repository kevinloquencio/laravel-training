@extends('layouts.app',[
'title' => 'Manage Employees'
])

@section('content')
<div class="container">


    @if( session('success') )
    <div class="alert alert-success" role="alert">
        {{ @session('success') }}
    </div>
    @endif

    <div class="card">
        <div class="card-header">
            {{ __('Manage Employees') }}
            <div class="float-right">
                <a href="{{ route('employees.create') }}">Create New Employee</a>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('employees.index') }}" class="form-inline pb-3" method="get">
            
                <div class="form-group">
                    <label for="q" class="sr-only">Search</label>
                    <div class="input-group">
                        <input class="form-control" value="{{ request()->q }}" type="text" name="q"
                            placeholder="Search...">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>

                <div class="form-group mx-sm-3"> 
                    <label for="limit" class="sr-only">Limit</label>
                    @component('components.input_limit')
                    @slot('limit', request()->limit ?? config('pagination.limit'))
                    @endcomponent
                </div>

            </form>

            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Company</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->full_name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>{{ $employee->company->name }}</td>
                        <td> 

                            <a href="{{ route('employees.edit', $employee->id) }}"
                                class="btn btn-warning btn-sm">Edit</a>

                            <a href="{{ route('employees.show' ,$employee->id) }}"
                                class="btn btn-primary btn-sm">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $employees->appends([ 'q' => request()->q, 'limit' => request()->limit ])->links() }}

        </div>
    </div>

</div>
@endsection
