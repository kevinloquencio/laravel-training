@extends('layouts.app',[
'title' => 'Edit Employee | '. $employee->full_name
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Edit Employee') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('employees.update', $employee->id) }}"
                        enctype="multipart/form-data">
                        @method('patch')
                        @csrf

                        @component('employees.form')

                        @slot('first_name', old('first_name') ?? $employee->first_name)
                        @slot('last_name', old('last_name') ?? $employee->last_name)
                        @slot('email', old('email') ?? $employee->email)
                        @slot('company_id', old('company_id') ?? $employee->company_id)
                        @slot('phone', old('phone') ?? $employee->phone)
                        @slot('companies', $companies)

                        @endcomponent

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                        <a href="{{ route('employees.show', $employee->id) }}">
                            {{ __('View Employee') }}
                        </a>
                        <br>
                        <a href="{{ route('employees.index') }}">
                            {{ __('Return Employee List') }}
                        </a>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
