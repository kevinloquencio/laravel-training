@extends('layouts.app',[
'title' => "Employee {$employee->full_name}"
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __( $employee->full_name ) }}</div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Name</dt>
                        <dd class="col-sm-9">{{ $employee->full_name }}</dd>

                        <dt class="col-sm-3">Email</dt>
                        <dd class="col-sm-9"><a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a></dd>

                        <dt class="col-sm-3">Phone</dt>
                        <dd class="col-sm-9"><a target="_blank"
                                href="tel:{{ $employee->phone }}">{{ $employee->phone }}</a>
                        </dd>
                        <dt class="col-sm-3">Company</dt>
                        <dd class="col-sm-9"><a
                                href="{{ route('companies.show', $employee->company_id) }}">{{ $employee->company->name }}</a>
                        </dd>

                        <dt class="col-sm-3">Last Edit</dt>
                        <dd class="col-sm-9 small">{{ $employee->updated_at }}</dd>


                    </dl>

                    <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-warning">Edit</a>

                    <form action="{{ route('employees.destroy', $employee->id) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>

                </div>
            </div>

            <div class="p-2"></div>
            <div class="card">
                <div class="card-header">Employee's Company</div>
                <div class="card-body">

                    @if($employee->company->image)
                    <img src="{{ $employee->company->image->url }}" alt="{{ $employee->company->name }} Logo">
                    @endif
                    <dl class="row">

                        <dt class="col-sm-3">Name</dt>
                        <dd class="col-sm-9">
                            <a title="View company" href="{{ route('companies.show', $employee->company_id) }}">
                                {{ $employee->company->name }}
                            </a>
                        </dd>

                        <dt class="col-sm-3">Email</dt>
                        <dd class="col-sm-9"><a
                                href="mailto:{{ $employee->company->email }}">{{ $employee->company->email }}</a></dd>

                        <dt class="col-sm-3">Website</dt>
                        <dd class="col-sm-9"><a target="_blank"
                                href="{{ $employee->company->website }}">{{ $employee->company->website }}</a></dd>

                    </dl>


                </div>
            </div>


        </div>
    </div>
</div>

@endsection
