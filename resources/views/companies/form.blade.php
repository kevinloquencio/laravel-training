<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
            value="{{ $name }}" autocomplete="name" required autofocus>

        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
            value="{{ $email }}" required autocomplete="email">

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="website" class="col-md-4 col-form-label text-md-right">{{ __('Website') }}</label>

    <div class="col-md-6">
        <input id="website" type="text" value="{{ $website }}"
            class="form-control @error('website') is-invalid @enderror" name="website" required>

        @error('website')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Logo') }}</label>

    <div class="col-md-6">
        <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" {{ $logo_attr ?? '' }} >
        <span class="small">Image must be 100x100 dimension and 2MB max file size</span>

        @error('image')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>