@extends('layouts.app',[
'title' => 'Edit Company | '. $company->name
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Edit Company') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('companies.update', $company->id) }}"
                        enctype="multipart/form-data">
                        @method('patch')
                        @csrf

                        @component('companies.form')

                        @slot('name', old('name') ?? $company->name)
                        @slot('email', old('email') ?? $company->email)
                        @slot('website', old('website') ?? $company->website)

                        @endcomponent

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                        <a href="{{ route('companies.show', $company->id) }}">
                            {{ __('View company') }}
                        </a>
                        <br>
                        <a href="{{ route('companies.index') }}">
                            {{ __('Return Company List') }}
                        </a>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
