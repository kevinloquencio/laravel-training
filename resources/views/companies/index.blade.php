@extends('layouts.app',[
'title' => 'Manage Companies'
])

@section('content')
<div class="container">


    @if( session('success') )
    <div class="alert alert-success" role="alert">
        {{ @session('success') }}
    </div>
    @endif

    <div class="card">
        <div class="card-header">
            {{ __('Manage Companies') }}
            <div class="float-right">
                <a href="{{ route('companies.create') }}">Create New Company</a>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('companies.index') }}" class="form-inline pb-3" method="get">
            
                <div class="form-group">
                    <label for="q" class="sr-only">Search</label>
                    <div class="input-group">
                        <input class="form-control" value="{{ request()->q }}" type="text" name="q"
                            placeholder="Search...">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>

                <div class="form-group mx-sm-3"> 
                    <label for="limit" class="sr-only">Limit</label>
                    @component('components.input_limit')
                    @slot('limit', request()->limit ?? config('pagination.limit'))
                    @endcomponent
                </div>

            </form>

            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($companies as $company)
                    <tr>
                        <td>
                            @if($company->image)
                            <img class="profile-image rounded-circle" src="{{ $company->image->url }}"
                                alt="{{ $company->name }} Logo">
                            @else
                            <div class="profile-image initials">{{ $company->name_initials }}</div>
                            @endif

                            <div>{{ $company->name }}</div>
                        </td>
                        <td>
                            <a href="mailto:{{ $company->email }}">{{ $company->email }}</a>
                        </td>
                        <td>
                            <a target="_blank" href="{{ $company->website }}">{{ $company->website }}</a>
                        </td>
                        <td>{{ $company->updated_at }}</td>
                        <td>

                            <a href="{{ route('companies.edit', $company->id) }}"
                                class="btn btn-warning btn-sm">Edit</a>

                            <a href="{{ route('companies.show' ,$company->id) }}"
                                class="btn btn-primary btn-sm">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $companies->appends([ 'q' => request()->q, 'limit' => request()->limit ])->links() }}
        </div>
    </div>
</div>
@endsection
