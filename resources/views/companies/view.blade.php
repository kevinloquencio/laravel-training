@extends('layouts.app',[
'title' => $company->name
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __( $company->name ) }}</div>
                <div class="card-body">

                    @if($company->image)
                    <img src="{{ $company->image->url }}" alt="{{ $company->name }} Logo">
                    @endif
                    <dl class="row">

                        <dt class="col-sm-3">Name</dt>
                        <dd class="col-sm-9">{{ $company->name }}</dd>

                        <dt class="col-sm-3">Email</dt>
                        <dd class="col-sm-9"><a href="mailto:{{ $company->email }}">{{ $company->email }}</a></dd>

                        <dt class="col-sm-3">Website</dt>
                        <dd class="col-sm-9"><a target="_blank"
                                href="{{ $company->website }}">{{ $company->website }}</a></dd>

                    </dl>

                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{{ route('companies.edit', $company->id) }}" class="btn btn-warning">Edit</a>
                        <form action="{{ route('companies.destroy', $company->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                    
                </div>
            </div>

            <div class="p-2"></div>

            <div class="card">
                <div class="card-header">
                    Employees
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->id }}</td>
                                <td>
                                    <a href="{{ route('employees.show', $employee->id) }}">
                                        {{ $employee->full_name }}
                                    </a>
                                </td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->phone }}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $employees->appends([ 'q' => request()->q ])->links() }}

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
