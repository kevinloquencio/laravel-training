@extends('layouts.app',[
    'title' => 'Create Company'
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Create Company') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('companies.store') }}" enctype="multipart/form-data">
                        @csrf
                        
                        @component('companies.form')

                            @slot('name', old('name'))
                            @slot('email', old('email'))
                            @slot('website', old('website'))

                            @slot('logo_attr','required')
                        @endcomponent


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                        <a href="{{ route('companies.index') }}">
                            {{ __('Return Company List') }}
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
