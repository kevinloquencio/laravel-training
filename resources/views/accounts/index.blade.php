@extends('layouts.app',[
'title' => 'Manage Accounts'
])

@section('content')
<div class="container">


    @if( session('success') )
    <div class="alert alert-success" role="alert">
        {{ @session('success') }}
    </div>
    @endif

    <div class="card">
        <div class="card-header">
            {{ __('Manage Accounts') }}
            <div class="float-right">
                <a href="{{ route('accounts.create') }}">Create New Account</a>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('accounts.index') }}" class="form-inline pb-3" method="get">

                <div class="form-group"> 
                    <div class="input-group">
                    <label for="q" class="sr-only">Search</label>

                        <input class="form-control" value="{{ request()->q }}" type="text" name="q"
                            placeholder="Search...">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>

                <div class="form-group mx-sm-3"> 
                    <label for="limit" class="sr-only">Limit</label>
                    @component('components.input_limit')
                    @slot('limit', request()->limit ?? config('pagination.limit'))
                    @endcomponent
                </div>

            </form>

            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($accounts as $account)
                    <tr>
                        <td>{{ $account->id }}</td>
                        <td>{{ $account->name }}</td>
                        <td>{{ $account->email }}</td>
                        <td>{{ $account->role_text }}</td>
                        <td>

                            <a href="{{ route('accounts.edit', $account->id) }}" class="btn btn-warning btn-sm">Edit</a>

                            <a href="{{ route('accounts.show' ,$account->id) }}" class="btn btn-primary btn-sm">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $accounts->appends([ 'q' => request()->q, 'limit' => request()->limit ])->links() }}

        </div>
    </div>

</div>
@endsection
