@extends('layouts.app',[
'title' => 'Create Account'
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Create Account') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('accounts.store') }}">
                        @csrf

                        @component('accounts.form')

                        @slot('name', old('name'))
                        @slot('email', old('email'))
                        @slot('role', old('role'))
                        @slot('roles', $roles)

                        @endcomponent

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>


                        <a href="{{ route('accounts.index') }}">
                            {{ __('Return Account List') }}
                        </a>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
