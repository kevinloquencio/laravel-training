@extends('layouts.app',[
'title' => 'Change Password'
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Change Password') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('accounts.update_password', $account->id) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Update Password</button>


                    </form>

                    <a href="{{ route('accounts.show', $account->id) }}">
                        {{ __('View Account') }}
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
