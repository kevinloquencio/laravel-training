@extends('layouts.app',[
'title' => 'Edit Account | '. $account->name
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if( session('success') )
            <div class="alert alert-success" role="alert">
                {{ @session('success') }}
            </div>
            @endif

            @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header">{{ __('Edit Account') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('accounts.update', $account->id) }}"
                        enctype="multipart/form-data">
                        @method('put')
                        @csrf

                        @component('accounts.form')

                        @slot('show_password_field', false)
                        @slot('name', old('name') ?? $account->name) 
                        @slot('email', old('email') ?? $account->email) 
                        @slot('role', old('role') ?? $account->role)
                        @slot('roles', $roles)

                        @endcomponent

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                        <a href="{{ route('accounts.show', $account->id) }}">
                            {{ __('View Account') }}
                        </a>
                        <br>
                        <a href="{{ route('accounts.index') }}">
                            {{ __('Return Account List') }}
                        </a>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
