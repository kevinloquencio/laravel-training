@extends('layouts.app',[
'title' => "Employee {$account->full_name}"
])

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        @if( session('warning') )
            <div class="alert alert-warning" role="alert">
                {{ @session('warning') }}
            </div>
            @endif
            
            <div class="card">
                <div class="card-header"><a title="Account Id"
                        href="{{ route('accounts.show', $account->id) }}">{{ $account->id }}</a> -
                    {{ __($account->name) }}</div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Name</dt>
                        <dd class="col-sm-9">{{ $account->name }}</dd>
                        <dt class="col-sm-3">Email</dt>
                        <dd class="col-sm-9"><a href="mailto:{{ $account->email }}">{{ $account->email }}</a></dd>
                        <dt class="col-sm-3">Role</dt>
                        <dd class="col-sm-9">{{ $account->role_text }}</dd>
                        <dt class="col-sm-3">Last Edit</dt>
                        <dd class="col-sm-9 small">{{ $account->updated_at }}</dd>
                    </dl>

                    <a href="{{ route('accounts.edit', $account->id) }}" class="btn btn-warning">Edit</a>
                    <a href="{{ route('accounts.change_password', $account->id) }}" class="btn btn-primary">Chage
                        Password</a>

                    <form action="{{ route('accounts.destroy', $account->id) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>



                    <a href="{{ route('accounts.index') }}">
                        {{ __('Return Account List') }}
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
