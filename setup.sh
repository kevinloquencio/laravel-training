
ENV_FILE=.env

# Check if the environment file exists since it is required during this setup
if [ ! -f "$ENV_FILE" ]; then
    echo "Generating none existence environment file..."
    cp .env.example .env
    echo "Env file generated, please configure the environment variables then run the setup.sh again."
else

# Install the required packages and seed necessary application data
composer install
php artisan config:cache
php artisan key:generate
php artisan migrate:fresh
php artisan db:seed
php artisan passport:client --personal --name=CompanyManagementPersonal

# Clear storage
rm -rfd /storage/app/public/logos

echo ""
echo ""
echo "Login to the application using the details below and don't forget to change the password"
echo "Username: admin@admin.com"
echo "Password: password"
fi