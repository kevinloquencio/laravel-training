# Laravel Training
A simple application tha can manage companies and employees within the companies
## Framework
Application is develope using [Laravel 7 Framework](https://laravel.com/docs/7.x)
## Getting Started
Application can run on local environment, vagrant (Laravel Homestead) or docker but recommended to run it on vagrant

- [Environment Setup](##-environment-setup)
- [Application Setup](####-application-setup)
- [Environment Variables](####-environment-variables)
- [Development Domain](##-application-development-domain)
- [Usage](##-usage)

#### Environment Setup
Setup homestead follow laravel [documentation](https://laravel.com/docs/7.x/homestead)
Once done provision vagrant based on the homestead project configuration ``Homestead.yml``
```
vagrant provision
```

#### Application Setup
Run bash ``setup.sh`` to install required packages and initialize data from seeders
#### Environment Variables
Run bash ``setup.sh`` from the root project and if the .env file does not exists it will automatically generate one and replace .env variables below:
```
APP_NAME="Company Management"
APP_ENV=testing
APP_URL=http://laraveltraining.test

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD="secret"
```
#### Application Development Domain
``local domain: laraveltraining.test``

Website: [http://laraveltraining.test](http://laraveltraining.test)

## Application Usage
Following links below can be used to manage the companies and employees as well as the accounts on the system.

### Simple Dashboard
Application has a simple dashboard that will show the count of companies, employees and accounts registered on the system it can be accessed in the home link of the application.

```
http://laraveltraining.test
```

### Company Management
Manage the companies from the link below and it will display a paginated list of registered companies on the application.

##### Company Listing Page
```
http://laraveltraining.test/companies
```
##### Create Company
Company creation can be accessed from the listing page by clicking the link ["Create New Company"](http://laraveltraining.test/companies/create) on the top of the listing box or follow the link below.
```
http://laraveltraining.test/companies/create
```
All fields are required when creating a company and the logo is required to be **100 x 100** dimension and maximum of **2MB** file size

Once done filling the required fields, click the "Save" button.

##### View Company
From the listing page, click the the "View" button to view the company details and its employees

##### Edit Company
From the listing page or the viewing page, click the the "Edit" button to edit the company details

All fields are required except the logo and if ever requires to change then the default rules will apply where logo must be **100 x 100** dimension and maximum of **2MB** file size.

Once done with the desired changes, click the "Save" button.

##### Delete Company
From the viewing page of the company, click the "Delete" button.

**Note** that when deleting the company will also delete the employees registered to that specific company.

### Employee Management
Manage the employees from the linke below and it will display a paginated list of the registered employees on the application.

##### Employee Listing Page
```
http://laraveltraining.test/employee
```
##### Create Employee
Employee creation can be accessed from the listing page by clicking the link ["Create New Employee"](http://laraveltraining.test/employees/create) on the top of the listing box or follow the link below.
```
http://laraveltraining.test/employees/create
```
All fields are required when creating an employee and identify which company does the employee employeed.

Once done filling the required fields, click the "Save" button.

##### View Employee
From the listing page, click the the "View" button to view the employee details and its company.

##### Edit Employee
From the listing page or the viewing page, click the the "Edit" button to edit the employee details

Still all the fields are required when updating the employee details.

Once done with the desired changes, click the "Save" button.

##### Delete Employee
From the viewing page of the employee, click the "Delete" button.


### Account Management
Manage the accounts from the linke below and the page will display a paginated list of registered user accounts on the application.
##### Account Listing Page
```
http://laraveltraining.test/accounts
```

##### Create Account
Account creation can be accessed from the listing page by clicking the link ["Create New Account"](http://laraveltraining.test/accounts/create) on the top of the listing box or follow the link below.
```
http://laraveltraining.test/accounts/create
```

All fields are required when creating an account and identify its [role](####-account-roles)

Once done filling the required fields, click the "Save" button.

##### View Account
From the listing page, click the the "View" button to view the account details.

##### Edit Account
From the listing page or the viewing page, click the the "Edit" button to edit the account details

Still all the fields are required except the [password](#####change-account-password) since the feature is separated from editing the account details.

Once done with the desired changes, click the "Save" button.

##### Change Account Password
From the viewing page of the account, click the "Change Password" button and input a new password and new password confirmation then click "Update Password" button.

##### Account Roles
Basically, only the accounts with administrator role can log-in and access the features but by default the application provided two roles which are the normal and administrator role. Normal role exists only to differentiate the access level of the user accounts and also to test that only that specific role can manage the entire application which is the administrator role.

- Normal
- Administrator
