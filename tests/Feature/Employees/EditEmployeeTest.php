<?php

namespace Tests\Feature\Employees;

class EditEmployeeTest extends SetupEmployee
{
    public function test_admin_can_access_edit_page()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.edit', $this->employee->id));
        $response->assertStatus(200);
        $response->assertViewHas('employee');
    }

    public function test_unauthorized_user_cannot_access_edit_page()
    {
        // With user authenticated access
        $response = $this->actingAs($this->user)->get(route('employees.edit', $this->employee->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_cannot_access_edit_page()
    {
        // Without user authenticated access
        $response = $this->get(route('employees.edit', $this->employee->id));
        $response->assertRedirect(route('login'));
    }

    public function test_employee_not_found_on_edit()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.edit', 0));
        $response->assertStatus(404);
    }
}
