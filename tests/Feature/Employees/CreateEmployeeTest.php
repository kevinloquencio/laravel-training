<?php

namespace Tests\Feature\Employees;

class CreateEmployeeTest extends SetupEmployee
{
    public function test_admin_can_access_create_page()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.create'));
        $response->assertStatus(200);
    }

    public function test_unauthorized_user_cannot_access_create_page()
    {
        // With user authenticated access
        $response = $this->actingAs($this->user)->get(route('employees.create'));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_cannot_access_create_page()
    {
        // Without user authenticated access
        $response = $this->get(route('employees.create'));
        $response->assertRedirect(route('login'));
    }
}
