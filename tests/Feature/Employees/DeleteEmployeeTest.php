<?php

namespace Tests\Feature\Employees;

class DeleteEmployeeTest extends SetupEmployee
{
    public function test_admin_can_delete()
    {
        $response = $this->actingAs($this->user_admin)->delete(route('employees.destroy', $this->employee->id));
        $response->assertRedirect(route('employees.index'));
        $response->assertSessionHas('success');
    }

    public function test_unauthorized_user_cannot_delete()
    {
        // With user authenticated access
        $response = $this->actingAs($this->user)->delete(route('employees.destroy', $this->employee->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_cannot_delete()
    {
        // Without user authenticated access
        $response = $this->delete(route('employees.destroy', $this->employee->id));
        $response->assertRedirect(route('login'));
    }

    public function test_employee_not_found_on_delete()
    {
        $response = $this->actingAs($this->user_admin)->delete(route('employees.destroy', 0));
        $response->assertStatus(404);
    }

}
