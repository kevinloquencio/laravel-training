<?php

namespace Tests\Feature\Employees;

use App\Company;
use App\Employee;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

abstract class SetupEmployee extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    
    protected $user;
    protected $user_admin;
    protected $company;
    protected $employee;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->state('role_normal')->create();

        $this->user_admin = factory(User::class)->state('role_admin')->create();

        $this->company = factory(Company::class)->create();

        $this->employee = factory(Employee::class)->create([
            'company_id' => $this->company->id
        ]);
    }
}
