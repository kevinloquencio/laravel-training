<?php

namespace Tests\Feature\Employees;

class ManageEmployeeTest extends SetupEmployee
{
    public function test_admin_can_browse()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.index'));
        $response->assertStatus(200);
        $response->assertViewHas('employees');
    }

    public function test_unauthorized_user_browsing()
    {
        // With user authenticated access
        $response = $this->actingAs($this->user)->get(route('employees.index'));
        $response->assertUnauthorized();
    }

    public function test_guest_unauthorized_browsing()
    {
        // Without user authenticated access
        $response = $this->get(route('employees.index'));
        $response->assertRedirect(route('login'));
    }
}
