<?php

namespace Tests\Feature\Employees;

use App\Employee;

class SaveEmployeeTest extends SetupEmployee
{
    public function test_admin_can_save()
    {
        $data = factory(Employee::class)->raw([
            'company_id' => $this->company->id
        ]);

        $response = $this->actingAs($this->user_admin)->post(route('employees.store'), $data);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('employees.create'));
        $response->assertSessionHas('success');
    }
    
    public function test_unauthorized_user_cannot_save()
    {
        $response = $this->actingAs($this->user)->post(route('employees.store'));
        $response->assertUnauthorized();
    }

    public function test_save_fields_required_validation_fail()
    {
        $data = [
            'first_name' => '',
            'last_name' => '',
            'company_id' => '',
            'email' => '',
            'phone' => ''
        ];

        $response = $this->actingAs($this->user_admin)->post(route('employees.store'), $data);
        $response->assertSessionHasErrors([
            'first_name' => 'The first name field is required.',
            'last_name' => 'The last name field is required.',
            'company_id' => 'The company id field is required.',
            'email' => 'The email field is required.',
            'phone' => 'The phone field is required.'
        ]);
    }

    public function test_company_invalid_validation_fail()
    {
        $data = factory(Employee::class)->raw([
            'company_id' => 0
        ]);

        $response = $this->actingAs($this->user_admin)->post(route('employees.store'), $data);
        $response->assertSessionHasErrors([
            'company_id' => 'The selected company id is invalid.'
        ]);
    }

    public function test_email_invalid_validation_fail()
    {
        $data = factory(Employee::class)->raw([
            'company_id' => $this->company->id,
            'email' => 'test'
        ]);

        $response = $this->actingAs($this->user_admin)->post(route('employees.store'), $data);
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    public function test_phone_length_validation_fail()
    {
        $data = factory(Employee::class)->raw([
            'company_id' => $this->company->id,
            'phone' => '1'
        ]);
        
        $response = $this->actingAs($this->user_admin)->post(route('employees.store'), $data);
        $response->assertSessionHasErrors([
            'phone' => 'The phone must be at least 9 characters.'
        ]);
    }
}
