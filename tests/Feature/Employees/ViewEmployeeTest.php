<?php

namespace Tests\Feature\Employees;

class ViewEmployeeTest extends SetupEmployee
{
    public function test_admin_can_view()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.show', $this->employee->id));
        $response->assertStatus(200);
        $response->assertViewHas('employee');
    }

    public function test_unauthorized_user_cannot_view()
    {
        // With user authenticated access
        $response = $this->actingAs($this->user)->get(route('employees.show', $this->employee->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_cannot_view()
    {
        // Without user authenticated access
        $response = $this->get(route('employees.show', $this->employee->id));
        $response->assertRedirect(route('login'));
    }

    public function test_employee_not_found_on_view()
    {
        $response = $this->actingAs($this->user_admin)->get(route('employees.show', 0));
        $response->assertStatus(404);
    }
}
