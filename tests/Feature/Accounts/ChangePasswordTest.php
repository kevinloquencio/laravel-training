<?php

namespace Tests\Feature\Accounts;

class ChangePasswordTest extends SetupAccount
{
    // Admin can change account password
    // Unauthorized user cannot change account password
    // Unauthenticated user cannot change account password
    // Not found account on change password

    /**
     * @test
     */
    public function admin_can_change_account_password()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.change_password', $this->account->id));
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_change_account_password()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.change_password', $this->account->id));
        $response->assertForbidden();
    }
 
    /**
     * @test
     */
    public function unauthenticated_user_cannot_change_account_password()
    {
        $response = $this->get(route('accounts.change_password', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function not_found_account_on_change_password()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.change_password', 0));
        $response->assertNotFound();
    }
}
