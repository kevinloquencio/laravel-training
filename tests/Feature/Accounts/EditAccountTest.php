<?php

namespace Tests\Feature\Accounts;

class EditAccountTest extends SetupAccount
{
    // Admin can edit account
    // Unauthorized user cannot edit account
    // Unauthenticated user cannot edit account
    // Not found account on edit

    /**
     * @test
     */
    public function admin_can_edit_account()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.edit', $this->account->id));
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_edit_account()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.edit', $this->account->id));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_edit_account()
    {
        $response = $this->get(route('accounts.edit', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function not_found_account_on_edit()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.edit', 0));
        $response->assertNotFound();
    }
}
