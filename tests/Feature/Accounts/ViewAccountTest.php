<?php

namespace Tests\Feature\Accounts;

class ViewAccountTest extends SetupAccount
{
    // Admin can view account
    // Unauthorized user cannot view account
    // Unauthenticated user cannot view account
    // Not found account on view

    /**
     * @test
     */
    public function admin_can_view_account()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.show', $this->account->id));
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_view_account()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.show', $this->account->id));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_view_account()
    {
        $response = $this->get(route('accounts.show', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function not_found_account_on_view()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.show', 0));
        $response->assertNotFound();
    }
}
