<?php

namespace Tests\Feature\Accounts;

class ManageAccountTest extends SetupAccount
{
    // Admin can access index
    // Unauthorized user access to index
    // Unauthenticated user cannot access index
    // Index returning paginated data


    /**
     * @test
     */
    public function admin_can_access_index()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.index'));
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function unauthorized_user_access_to_index()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.index'));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_access_index()
    {
        $response = $this->get(route('accounts.index'));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function index_returning_paginated_data()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.index'));
        $response->assertSuccessful();
        $response->assertViewHas('accounts');
        
    }
}
