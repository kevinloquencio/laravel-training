<?php

namespace Tests\Feature\Accounts;

use App\User;

class StoreAccountTest extends SetupAccount
{
    // Fields required validation fail
    // Email invalid validation fail
    // Role invalid validation fail
    // Admin can successfully store account
    // Unauthorized user cannot store account
    // Unauthenticated user cannot store account

    /**
     * @test
     */
    public function fields_required_validation_fail()
    {
        // Produce data to fail in validation
        $account = [
            'name' => '',
            'email' => '',
            'password' => '',
            'password_confirmation' => '',
            'role' => ''
        ];

        $response = $this->actingAs($this->user_admin)->post(route('accounts.store'), $account);
        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'email' => 'The email field is required.',
            'password' => 'The password field is required.',
            'role' => 'The role field is required.',
        ]);
    }

    /**
     * @test
     */
    public function email_invalid_validation_fail()
    {
        $data = factory(User::class)->states('role_admin', 'test')->raw([
            'email' => 'dummy'
        ]);
        $response = $this->actingAs($this->user_admin)->post(route('accounts.store'), $data);
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    /**
     * @test
     */
    public function role_invalid_validation_fail()
    {
        $data = factory(User::class)->states('role_admin', 'test')->raw([
            'role' => $this->faker()->name()
        ]);

        $response = $this->actingAs($this->user_admin)->post(route('accounts.store'), $data);
        $response->assertSessionHasErrors([
            'role' => 'The selected role is invalid.'
        ]);
    }

    /**
     * @test
     */
    public function admin_can_successfully_store_account()
    {
        $account = factory(User::class)->states('role_normal', 'test')->raw();

        $response = $this->actingAs($this->user_admin)->post(route('accounts.store'), $account);
        $response->assertRedirect(route('accounts.create'));
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_store_account()
    {
        $response = $this->actingAs($this->user)->post(route('accounts.store'));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_store_account()
    {
        $response = $this->post(route('accounts.store'));
        $response->assertRedirect(route('login'));
    }

}
