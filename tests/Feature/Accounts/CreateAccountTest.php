<?php

namespace Tests\Feature\Accounts;

class CreateAccountTest extends SetupAccount
{
    // Admin can create account
    // Unauthorized user cannot create account
    // Unauthenticated user cannot create account

    /**
     * @test
     */
    public function admin_can_create_account()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.create'));
        $response->assertSuccessful();
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_create_account()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.create'));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_create_account()
    {
        $response = $this->get(route('accounts.create'));
        $response->assertRedirect(route('login'));
    }
}
