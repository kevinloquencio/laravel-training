<?php

namespace Tests\Feature\Accounts;

class UpdatePasswordTest extends SetupAccount
{
 
    // Admin can update account password
    // Unauthorized user cannot update account password
    // Unauthenticated user cannot update account password
    // Password does not match validation failed
    // Password min character validation failed
    // Password required validation failed
    // Not found account on update password


    protected function password()
    {
        return "jondoe!@#!";
    }

    /**
     * @test
     */
    public function admin_can_update_account_password()
    {
        $password = [
            'password' => $this->password(),
            'password_confirmation' => $this->password()
        ];

        $response = $this->actingAs($this->user_admin)->post(route('accounts.update_password', $this->account->id), $password);
        $response->assertRedirect(route('accounts.update_password', $this->account->id));
    }

    /**
     * @test
     */
    public function unauthorized_user_cannot_update_account_password()
    {
        $response = $this->actingAs($this->user)->get(route('accounts.update_password', $this->account->id));
        $response->assertForbidden();
    }
 
    /**
     * @test
     */
    public function unauthenticated_user_cannot_test_account_password()
    {
        $response = $this->get(route('accounts.update_password', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function password_not_match_validation_failed()
    {
        $password = [
            'password' => $this->password(). "123",
            'password_confirmation' => $this->password()
        ];

        $response = $this->actingAs($this->user_admin)->post(route('accounts.update_password', $this->account->id), $password);
        $response->assertSessionHasErrors([
            'password' => 'The password confirmation does not match.'
        ]);
    }

    /**
     * @test
     */
    public function password_min_character_validation_failed()
    {
        $password = [
            'password' => "1",
            'password_confirmation' => "1"
        ];

        $response = $this->actingAs($this->user_admin)->post(route('accounts.update_password', $this->account->id), $password);
        $response->assertSessionHasErrors([
            'password' => 'The password must be at least 8 characters.'
        ]);
    }


    /**
     * @test
     */
    public function password_required_validation_failed()
    {
        $password = [
            'password' => "",
            'password_confirmation' => ""
        ];

        $response = $this->actingAs($this->user_admin)->post(route('accounts.update_password', $this->account->id), $password);
        $response->assertSessionHasErrors([
            'password' => 'The password field is required.'
        ]);
    }

    /**
     * @test
     */
    public function not_found_account_on_change_password()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.update_password', 0));
        $response->assertNotFound();
    }
}
