<?php

namespace Tests\Feature\Accounts;

use App\User;

class UpdateAccountTest extends SetupAccount
{
    // Fields required validation fail
    // Email invalid validation fail
    // Role invalid validation fail
    // Admin can successfully update account
    // Unauthorized user cannot update account
    // Unauthenticated user cannot update account
    // Not found account on update

    /**
     * @test
     */
    public function fields_required_validation_fail()
    {
        // Produce data to fail in validation
        $account = [
            'name' => '',
            'email' => '',
            'role' => ''
        ];

        $response = $this->actingAs($this->user_admin)->patch(route('accounts.update', $this->account->id), $account);
        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'email' => 'The email field is required.',
            'role' => 'The role field is required.',
        ]);
    }

    /**
     * @test
     */
    public function email_invalid_validation_fail()
    {
        $data = factory(User::class)->states('role_admin', 'test')->raw([
            'email' => 'dummy'
        ]);

        $response = $this->actingAs($this->user_admin)->patch(route('accounts.update', $this->account->id), $data);
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    /**
     * @test
     */
    public function role_invalid_validation_fail()
    {
        $data = factory(User::class)->states('role_admin', 'test')->raw([
            'role' => $this->faker()->name()
        ]);

        $response = $this->actingAs($this->user_admin)->patch(route('accounts.update', $this->account->id), $data);
        $response->assertSessionHasErrors([
            'role' => 'The selected role is invalid.'
        ]);
    }


    /**
     * @test
     */
    public function admin_can_successfully_update_account()
    {
        $account = factory(User::class)->states('role_normal', 'test')->raw();

        $response = $this->actingAs($this->user_admin)->patch(route('accounts.update', $this->account->id), $account);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('accounts.edit', $this->account->id));
    }

 
    /**
     * @test
     */
    public function unauthorized_user_cannot_update_account()
    {
        $response = $this->actingAs($this->user)->patch(route('accounts.update', $this->account->id));
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function unauthenticated_user_cannot_update_account()
    {
        $response = $this->patch(route('accounts.update', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function not_found_account_on_update()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.update', 0));
        $response->assertNotFound();
    }
}