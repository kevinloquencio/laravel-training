<?php

namespace Tests\Feature\Accounts;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

abstract class SetupAccount extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    protected $user_admin;
    protected $user;
    protected $account;

    protected function setUp() :void
    {
        parent::setUp();

        $this->user = factory(User::class)->state('role_normal')->create();
        $this->user_admin = factory(User::class)->state('role_admin')->create();

        $this->account = factory(User::class)->state('role_normal')->create();
    }
}
