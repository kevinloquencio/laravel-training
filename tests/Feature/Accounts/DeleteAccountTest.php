<?php

namespace Tests\Feature\Accounts;

class DeleteAccountTest extends SetupAccount
{
    // Admin can delete account
    // Unauthorized user cannot delete account
    // Unauthenticated user cannot delete account
    // Not found account on delete

    /**
     * @test
     */
    public function admin_can_delete_account()
    {
        $response = $this->actingAs($this->user_admin)->delete(route('accounts.destroy', $this->account->id));
        $response->assertRedirect(route('accounts.index'));
    }
 
    /**
     * @test
     */
    public function unauthorized_user_cannot_delete_account()
    {
        $response = $this->actingAs($this->user)->delete(route('accounts.destroy', $this->account->id));
        $response->assertForbidden();
    }

   /**
    * @test
    */
    public function unauthenticated_user_cannot_delete_account()
    {
        $response = $this->get(route('accounts.destroy', $this->account->id));
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function not_found_account_on_delete()
    {
        $response = $this->actingAs($this->user_admin)->get(route('accounts.destroy', 0));
        $response->assertNotFound();
    }
}
