<?php

namespace Tests\Feature\Accounts;

use App\User;
use Tests\TestCase;

class AdministratorAccountTest extends TestCase
{
    /**
     * @test
     */
    public function must_have_at_least_one_administrator_on_update()
    {
        // Delete all admin account
        User::whereRole(User::ROLE_ADMIN)->delete();
        // Create admin account
        $account = factory(User::class)->state('role_admin')->create();
        $data = factory(User::class)->state('role_normal')->raw();

        $response = $this->actingAs($account)->patch(route('accounts.update', $account->id), $data);
        $response->assertSessionHas("warning", "Sorry but the system must have at least one administrator account.");
    }

    /**
     * @test
     */
    public function must_have_at_least_one_administrator_on_delete()
    {
        // Delete all admin account
        User::whereRole(User::ROLE_ADMIN)->delete();
        // Create admin account
        $account = factory(User::class)->state('role_admin')->create();
        $data = factory(User::class)->state('role_normal')->raw();

        $response = $this->actingAs($account)->delete(route('accounts.update', $account->id), $data);
        $response->assertSessionHas("warning", "Sorry but the system must have at least one administrator account.");
    }
}