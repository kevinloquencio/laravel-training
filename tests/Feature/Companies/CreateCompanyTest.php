<?php

namespace Tests\Feature\Companies;

class CreateCompanyTest extends SetupCompany
{
    public function test_admin_can_access_create_page()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.create'));
        $response->assertStatus(200);
    }

    public function test_unauthorized_user_can_access_create_page()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->get(route('companies.create'));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_access_create_page()
    {
        // Without user
        $response = $this->get(route('companies.create'));
        $response->assertRedirect(route('login'));
    }
}
