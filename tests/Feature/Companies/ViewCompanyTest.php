<?php

namespace Tests\Feature\Companies;

class ViewCompanyTest extends SetupCompany
{
    public function test_admin_can_view()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.show',$this->company->id));
        $response->assertStatus(200);
        $response->assertViewHas('company');
        $response->assertViewHas('employees');
    }

    public function test_unauthorized_user_can_view()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->get(route('companies.show',$this->company->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_view()
    {
        // Without user
        $response = $this->get(route('companies.show',$this->company->id));
        $response->assertRedirect(route('login'));
    }

    public function test_company_not_found_on_view()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.show', 0));
        $response->assertNotFound();
    }
}
