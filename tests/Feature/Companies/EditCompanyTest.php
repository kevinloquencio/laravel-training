<?php

namespace Tests\Feature\Companies;

class EditCompanyTest extends SetupCompany
{
    public function test_admin_can_access_edit_page()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.edit', $this->company->id));
        $response->assertStatus(200);
        $response->assertViewHas('company');
    }

    public function test_unauthorized_user_can_access_edit_page()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->get(route('companies.edit', $this->company->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_access_edit_page()
    {
        // Without user
        $response = $this->get(route('companies.edit', $this->company->id));
        $response->assertRedirect(route('login'));
    }
    
    public function test_company_not_found_on_edit()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.edit', 0));
        $response->assertNotFound();
    }
}
