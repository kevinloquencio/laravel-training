<?php

namespace Tests\Feature\Companies;

use App\Company;
use Illuminate\Http\UploadedFile;

class UpdateCompanyTest extends SetupCompany
{
    public function test_admin_can_update()
    {
        $file = UploadedFile::fake()->image('avatar.jpg',100,100);
        
        $data = factory(Company::class)->raw([
            'image' => $file
        ]);

        $response = $this->actingAs($this->user_admin)->patch(route('companies.update', $this->company->id), $data);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('companies.edit', $this->company->id));
        $response->assertSessionHas('success');
    }

    public function test_unauthorized_user_can_update()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->patch(route('companies.update', $this->company->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_update()
    {
        // Without user
        $response = $this->patch(route('companies.update', $this->company->id));
        $response->assertRedirect(route('login')); 
    }

    public function test_update_no_changes()
    {
        $data = [
            'name'    => $this->company->name,
            'email'   => $this->company->email,
            'website' => $this->company->website
        ];

        $response = $this->actingAs($this->user_admin)->patch(route('companies.update', $this->company->id), $data);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('companies.edit', $this->company->id));
        $response->assertSessionHas('warning');
    }

    public function test_update_validation_fail()
    {
        // Produce data that will fail in validation
        $file = UploadedFile::fake()->image('avatar.jpg',200,200);
        $data = [
            'name'    => $this->faker->name(),
            'email'   => $this->faker->name(),
            'website' => "",
            'image'   => $file
        ];

        $response = $this->actingAs($this->user_admin)->patch(route('companies.update', $this->company->id), $data);
        $response->assertSessionHasErrors();
    }

    public function test_company_not_found_on_update()
    {
        $response = $this->actingAs($this->user_admin)->patch(route('companies.update', 0));
        $response->assertNotFound();
    }
}
