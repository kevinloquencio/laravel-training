<?php

namespace Tests\Feature\Companies;

class ManageCompanyTest extends SetupCompany
{
    public function test_admin_can_browse_company()
    {
        $response = $this->actingAs($this->user_admin)->get(route('companies.index'));
        $response->assertStatus(200);
        $response->assertViewHas('companies');
    }

    public function test_unauthorized_user_company_browsing()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->get(route('companies.index'));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_company_browsing()
    {
        // Without user
        $response = $this->get(route('companies.index'));
        $response->assertRedirect(route('login'));
    }
}
