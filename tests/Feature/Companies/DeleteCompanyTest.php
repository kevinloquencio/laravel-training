<?php

namespace Tests\Feature\Companies;

class DeleteCompanyTest extends SetupCompany
{
    public function test_admin_can_delete()
    {
        $response = $this->actingAs($this->user_admin)->delete(route('companies.destroy', $this->company->id));
        $response->assertRedirect(route('companies.index'));
        $response->assertSessionHas('success');
    }

    public function test_unauthorized_user_can_delete()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->delete(route('companies.destroy', $this->company->id));
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_delete()
    {
        // Without user
        $response = $this->delete(route('companies.destroy', $this->company->id));
        $response->assertRedirect(route('login'));
    }

    public function test_company_not_found_on_delete()
    {
        $response = $this->actingAs($this->user_admin)->delete(route('companies.destroy', 0));
        $response->assertNotFound();
    }
}
