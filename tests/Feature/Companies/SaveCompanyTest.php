<?php

namespace Tests\Feature\Companies;

use App\Company;
use Illuminate\Http\UploadedFile;

class SaveCompanyTest extends SetupCompany
{
    public function test_admin_can_save()
    {
        $file = UploadedFile::fake()->image('avatar.jpg',100,100);

        $data = factory(Company::class)->raw([
            'image' => $file
        ]);

        $response = $this->actingAs($this->user_admin)->post(route('companies.store'), $data);
        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('companies.create'));
        $response->assertSessionHas('success');

    }

    public function test_unauthorized_user_can_save()
    {
        // With authenticated normal user
        $response = $this->actingAs($this->user)->post(route('companies.store'), []);
        $response->assertUnauthorized();
    }

    public function test_unauthorized_guest_user_can_save()
    {
        // Without user
        $response = $this->post(route('companies.store'), []);
        $response->assertRedirect(route('login'));
    }

    public function test_save_validation_fail()
    {
        // Produce data that will fail in validation
        $file = UploadedFile::fake()->image('avatar.jpg',200,200);
        $data = [
            'name'    => $this->faker->name(),
            'email'   => $this->faker->name(),
            'website' => "",
            'image'   => $file
        ];

        $response = $this->actingAs($this->user_admin)->post(route('companies.store'), $data);
        $response->assertSessionHasErrors();
    }
}