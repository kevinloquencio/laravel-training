<?php

namespace Tests\Feature\Companies;

use App\Company;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

abstract class SetupCompany extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $user;
    protected $user_admin;
    protected $company;

    protected function setUp(): void
    {
        parent::setUp();

        // Create a normal user
        $this->user = factory(User::class)->state('role_normal')->create();

        // Create an administrator user
        $this->user_admin = factory(User::class)->state('role_admin')->create();

        $this->company = factory(Company::class)->create();
    }
}
