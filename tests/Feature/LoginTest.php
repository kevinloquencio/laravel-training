<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private $user_password;
    private $admin_email;
    private $normal_email;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user_password = $this->faker()->password(8);
        $this->admin_email = $this->faker()->safeEmail();
        $this->normal_email = $this->faker()->safeEmail();


        factory(User::class)->state('role_admin')->create([
            'email' => $this->admin_email,
            'password' => bcrypt($this->user_password)
        ]);

        factory(User::class)->state('role_normal')->create([
            'email' => $this->normal_email,
            'password' => bcrypt($this->user_password)
        ]);
    }

    /**
     * @test
     */
    public function only_administrator_can_login()
    {
        $response = $this->post(route('login'),[
            'email' => $this->admin_email,
            'password' => $this->user_password
        ]);

        $response->assertSessionHasNoErrors();
    }

    /**
     * @test
     */
    public function normal_user_cannot_login()
    {
        $response = $this->post(route('login'),[
            'email' => $this->normal_email,
            'password' => $this->user_password
        ]);

        $response->assertSessionHasErrors([
            'email' => 'These credentials do not match our records.'
        ]);
    }
}
