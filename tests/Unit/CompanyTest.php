<?php

namespace Tests\Unit;

use App\Helpers\ImageUploader;
use Tests\TestCase;
use App\Company;
use App\Helpers\ImageStorage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class CompanyTest extends TestCase
{
    /**
     * Check if the logo/image will be deleted from storage when the company is deleted
     * 
     * Note: Image deletion from storage is binded in model event observer "deleted" of the \App\Company Model
     * 
     */
    public function test_upload_and_remove_logos()
    {
        $storage = ImageStorage::disk();

        // Create company and upload test logo
        $file = new UploadedFile(resource_path('files/test.jpg'),"test.jpg");
        $company = factory(Company::class)->create();
        $uploader = new ImageUploader($file, $company);
        $uploader->save();

        // Get the company image or logo
        $image = $company->image;

        # Check if file exists
        $storage->assertExists($image->path);

        // Delete company
        $company->delete();

        # Check if file remove
        $storage->assertMissing($image->path);
    }
}
