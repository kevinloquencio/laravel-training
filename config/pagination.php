<?php

return [

    /**
     * Data limit per page
     */
    'limit' => env('PAGINATION_LIMIT', 10),

    /**
     * Max data limit per page
     */
    'max_limit' => env('PAGINATION_MAX_LIMIT', 10)

];
