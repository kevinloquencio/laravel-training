<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_ADMIN = 'admin';
    const ROLE_NORMAL = 'normal';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdministrator()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function getRoleTextAttribute()
    {
        switch ($this->role) {
            case static::ROLE_NORMAL:
                $role = 'Normal';
                break;

            case static::ROLE_ADMIN:
                $role = 'Administrator';
                break;
            default:
                $role = '';
                break;
        }
        
        return $role;
    }
}
