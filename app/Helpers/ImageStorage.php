<?php
namespace App\Helpers;

use Storage;
use App\Image;

class ImageStorage
{
    /**
     * Default disk for the image storage
     * 
     * @return Storage
     */
    public static function disk()
    {
        return Storage::disk('public');
    }

    /**
     * Delete an image from the default image storage disk
     * 
     * @param \App\Image $image
     */
    public static function delete(Image $image)
    {
        $storage = static::disk();
        return $storage->delete($image->path);
    }
}