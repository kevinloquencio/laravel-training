<?php
namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ImageStorage;
use Illuminate\Support\Carbon;

class ImageUploader
{
    public $storage;
    private $file;
    private $model;

    /**
     * @param $file The image file to be uploaded
     * @param $model Related model instance of to be uploaded image
     */
    public function __construct(UploadedFile $file, Model $model)
    {
        $this->storage = ImageStorage::disk();
        $this->file = $file;
        $this->model = $model;
    }

    /**
     * Save the file to the storage disk
     * 
     * @return App/Image Instance of the image model
     */
    public function save()
    {
        $path = $this->storage->putFileAs("logos", $this->file, $this->uniqueFileName());
        return $this->saveToDb($path, $this->file->getClientOriginalName());
    }

    /**
     * Generate unique file name
     * 
     * @return String
     */
    public function uniqueFileName()
    {
        $timestamp = Carbon::now()->timestamp;
        return $this->model->id. "_" .$timestamp. "." . $this->file->getClientOriginalExtension();
    }

    /**
     * Store the image information to database
     * Note: Model instance must have a oneMorph relationship of the Image model
     * 
     * @param $path Path of the image from the storage disk
     * @param $original_name The original name of the image
     * @return App/Image Instance of the image model
     */
    private function saveToDb($path, $original_name)
    {
        if( ! method_exists($this->model,'image')) throw new \Exception("Model instance must have the morphOne relationship named image from image model.");

        return $this->model->image()->create([
            'original_name' => $original_name,
            'path' => $path
        ]);
    }
}