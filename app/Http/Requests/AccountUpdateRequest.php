<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class AccountUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = implode(",", [User::ROLE_ADMIN, User::ROLE_NORMAL]);

        return [
            'name' => ['required', 'string', 'max:255'],
            'email'   => ['required', 'email', 'min:5', 'max:255', "unique:users,email,{$this->account->id}"],
            'role' => ['required', 'string', "in:{$roles}"],
        ];
    }

    public function data()
    {
        return $this->except(['_token']);
    }
}
