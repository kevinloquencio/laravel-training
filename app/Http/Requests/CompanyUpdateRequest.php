<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => ['required','string'],
            'email'   => ['required', 'email', "unique:companies,email,{$this->company->id}"],
            'website' => ['required', 'url'],
            'image'   => ['nullable', 'image', 'dimensions:max_width=100,max_height=100', 'mimes:jpeg,bmp,png'],
        ];
    }
    
    public function data()
    {
        return $this->except(['_token', 'image']);
    }
}
