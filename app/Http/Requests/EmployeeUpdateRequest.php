<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required','string'],
            'last_name'  => ['required','string'],
            'company_id' => ['required','numeric','exists:companies,id'],
            'email'      => ['required','email','unique:employees,email,' . $this->employee->id ],
            'phone'      => ['required','string','min:9']
        ];
    }
}
