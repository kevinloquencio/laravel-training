<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get pagination limit
     */
    public function getLimit()
    {
        $limit  = $this->limit ?? config('pagination.limit');

        // Enforce to not exceed page data based on max limit from config
        if($limit >= config('pagination.max_limit'))
            return config('pagination.max_limit');

        return $limit;
    }
}
