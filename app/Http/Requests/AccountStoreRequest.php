<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class AccountStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roles = implode(",", [User::ROLE_ADMIN, User::ROLE_NORMAL]);

        return [
            'name' => ['required', 'string', 'min:1', 'max:255'],
            'email' => ['required', 'string', 'email', 'min:5', 'max:255', 'unique:users'],
            'role' => ['required', 'string', "in:{$roles}"],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function data()
    {
        return $this->except(['_token']);
    }
}
