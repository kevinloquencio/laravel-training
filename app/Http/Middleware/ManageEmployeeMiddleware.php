<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ManageEmployeeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        abort_if(!$user->can('manage-app'), 401);

        return $next($request);
    }
}
