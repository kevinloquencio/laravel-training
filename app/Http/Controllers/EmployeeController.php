<?php

namespace App\Http\Controllers;

use App\{Company, Employee};
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Http\Requests\IndexRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $keyword = $request->q;
        $limit = $request->getLimit();

        $employees = Employee::on();

        if($keyword)
        {
            $employees->where(function($employees) use($keyword){

                $employees->orWhereRaw("concat(first_name, '', last_name ) like '%{$keyword}%'")
                        ->orWhereRaw("concat(last_name, '', first_name ) like '%{$keyword}%'")
                        ->orWhere('email','like',"%$keyword%");

            });
        }

        $employees = $employees->paginate($limit);

        return view('employees.index', [ 'employees' => $employees ] );
    }

    /**
     * Show the form for creating a new employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();

        return view('employees.create', [ 'companies' => $companies ]);
    }

    /**
     * Store a newly created employee
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {        
        $employee = new Employee;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->save();
        
        return redirect()->route('employees.create')->with('success','Employee saved successfully.');
    }

    /**
     * Display the specified employee
     *
     * @param  int  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.view', [ 'employee' => $employee ] );
    }

    /**
     * Show the form for editing the employee
     *
     * @param  int  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $companies = Company::all();

        return view('employees.edit', [ 'companies' => $companies, 'employee' => $employee ] );
    }

    /**
     * Update employee information
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->save();

        return redirect()->route('employees.edit', $employee->id)->with('success','Employee information updated.');
    }

    /**
     * Delete the employee from the database
     *
     * @param  int  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employees.index')->with('success','Employee deleted.');
    }
}
