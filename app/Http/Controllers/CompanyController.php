<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helpers\ImageUploader;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Requests\IndexRequest;
use DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of companies
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $keyword = $request->q;
        $limit = $request->getLimit();

        $companies = Company::on();

        if($keyword)
        {
            $companies->where(function($companies) use($keyword){

                $companies->orWhere('name','like',"%$keyword%")
                        ->orWhere('email','like',"%$keyword%");

            });
        }

        $companies = $companies->paginate($limit);

        return view('companies.index', [ 'companies'=> $companies]);
    }

    /**
     * Show the form for creating a new company.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created company in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        DB::beginTransaction();

        $company = Company::create($request->data());
        $upload = new ImageUploader($request->image, $company);
        $upload->save();

        DB::commit();

        return redirect()->route('companies.create')->with('success','Company successfully saved.');
    }

    /**
     * Display the specified company.
     *
     * @param  int  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $employees = $company->employees()->paginate(config('pagination.limit'));

        return view('companies.view', [
            'company' => $company,
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for editing the specified company.
     *
     * @param  int  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('companies.edit', [
            'company' => $company
        ]);
    }

    /**
     * Update the specified company in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $company->update($request->data());

        if( ! $company->wasChanged() && ! $request->image)
        {
            return redirect()->route('companies.edit', $company->id)
                             ->with('warning', 'There are no changes made');
        }

        if($request->image)
        {
            $upload = new ImageUploader($request->image, $company);
            $upload->save();
        }

        return redirect()->route('companies.edit', $company->id)
                         ->with('success', 'Company successfully updated.');
    }

    /**
     * Remove the specified company from storage.
     *
     * @param  int  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->route('companies.index')->with('success','Company successfully deleted.');
    }
}
