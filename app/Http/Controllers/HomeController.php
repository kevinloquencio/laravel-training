<?php

namespace App\Http\Controllers;

use App\{Company, Employee, User};

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $employee_count = Employee::count();
        $company_count = Company::count();
        $user_count = User::count();

        $user = auth()->user();

        return view('home', [
            'company_count' => $company_count,
            'employee_count' => $employee_count,
            'user_count' => $user_count,
            'user' => $user
        ]);
    }
}
