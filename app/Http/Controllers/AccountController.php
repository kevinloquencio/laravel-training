<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountStoreRequest;
use App\Http\Requests\AccountUpdatePasswordRequest;
use App\Http\Requests\AccountUpdateRequest;
use App\Http\Requests\IndexRequest;
use App\User;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $keyword = $request->q;
        $limit = $request->getLimit();

        $accounts = User::on();

        if($keyword)
        {
            $accounts->where(function($accounts) use($keyword){

                $accounts->orWhere('name','like',"%{$keyword}%")
                        ->orWhere('email','like',"%$keyword%");

            });
        }

        $accounts = $accounts->paginate($limit);

        return view('accounts.index', [ 'accounts' => $accounts ] );
    }

    /**
     * Show the form for creating a new account.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->roles();

        return view('accounts.create', [ 'roles' => $roles ]);
    }

    /**
     * Store a newly created account
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountStoreRequest $request)
    {
        $account = User::create($request->data());
        return redirect()->route('accounts.create')->with('success','User saved successfully.');
    }

    /**
     * Display the specified account
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $account)
    {
        return view('accounts.view', [ 'account' => $account ] );
    }

    /**
     * Show the form for editing the account
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account)
    {
        $roles = $this->roles();

        return view('accounts.edit', [ 'roles' => $roles, 'account' => $account ] );
    }

    /**
     * Update account information
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountUpdateRequest $request, User $account)
    {
        $account->name = $request->name;
        $account->email = $request->email;
        $account->role = $request->role;

        if($account->isDirty('role') && $account->role == User::ROLE_NORMAL)
        {
            if($this->hasOnlyOneAdmin())
                return redirect()->route('accounts.edit', $account->id)
                                ->withInput()
                                ->with('warning', 'Sorry but the system must have at least one administrator account.');
        }
        
        $account->save();

        return redirect()->route('accounts.edit', $account->id)->with('success','User information updated.');
    }

    /**
     * Delete the account from the database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account)
    {
        if($this->hasOnlyOneAdmin() && $account->role == User::ROLE_ADMIN)
            return redirect()->route('accounts.show', $account->id)
                        ->with('warning', 'Sorry but the system must have at least one administrator account.');

        $account->delete();

        return redirect()->route('accounts.index')->with('success','User deleted.');
    }

    /**
     * Show the account change password form
     * 
     * @param \App\User $account
     * 
     * @return \Illuminate\Http\Response
     */
    public function showChangePassword(User $account)
    {
        return view('accounts.change_password', ['account' => $account]);
    }

    /**
     * Update the account password
     * 
     * @param \App\Http\Requests\AccountUpdatePasswordRequest $request
     * @param \App\User $account
     * 
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(AccountUpdatePasswordRequest $request, User $account)
    {
        $account->password = bcrypt($request->password);
        $account->save();

        return redirect()->route('accounts.change_password', $account->id)->with('success', 'Account password updated.');
    }

    /**
     * Generate account role list
     * 
     * @return Array
     */
    public function roles()
    {
        return [
            'Administrator' => User::ROLE_ADMIN,
            'Normal' => User::ROLE_NORMAL
        ];
    }

    /**
     * 
     */
    public function hasOnlyOneAdmin()
    {
        if(User::whereRole(User::ROLE_ADMIN)->count() == 1)
            return true;

        return false;
    }
}
