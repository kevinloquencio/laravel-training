<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => (int) $this->id,
            'first_name' => $this->first_name, 
            'last_name'  => $this->last_name, 
            'full_name'  => $this->full_name,
            'company'    => $this->company->name, 
            'email'      => $this->email, 
            'phone'      => $this->phone,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
