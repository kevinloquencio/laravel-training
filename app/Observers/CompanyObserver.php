<?php

namespace App\Observers;

use App\Company;
use App\Helpers\ImageStorage;

class CompanyObserver
{
    /**
     * Handle the company "deleted" event.
     *
     * @param  \App\Company  $company
     * @return void
     */
    public function deleted(Company $company)
    {
        // Delete the images or logos when the company deleted
        $company->images->each(function($image){

            if( ImageStorage::delete($image)) // Remove from storage
                $image->delete(); // Remove from db

        });
    }
}
