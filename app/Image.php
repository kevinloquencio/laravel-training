<?php

namespace App;

use App\Helpers\ImageStorage;
use App\Helpers\ImageUploader;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'path',
        'original_name',
    ];
    
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute()
    {
        return ImageStorage::disk()->url($this->path);
    }
}
