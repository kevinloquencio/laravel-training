<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name', 'email', 'website'
    ];

    public function employees()
    {
        return $this->hasMany('App\Employee','company_id');
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable')->latest();
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getHasLogoAttribute()
    {
        return $this->image != null;
    }

    public function getHasEmployeesAttribute()
    {
        return $this->employees()->count() != 0;
    }

    /**
     * Return the first and last words initial characters in the name attribute
     * If contains one word, return the single letter
     */
    public function getNameInitialsAttribute()
    {
        $names = explode(" ", $this->name);
        $count_names = count($names);
        
        $first_in = substr($names[0], 0, 1); // First initial

        if($count_names == 1)
            return $first_in;

        return strtoupper($first_in .  substr($names[count($names)-1], 0, 1));
    }
}