<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {

    $name = $faker->company();

    return [
        'name'    => $name, 
        'email'   => $faker->safeEmail(),
        'website' => "https://{$faker->domainName}",
    ];
});