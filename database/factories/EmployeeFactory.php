<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'first_name' => $this->faker->name(),
        'last_name'  => $this->faker->name(),
        'email'      => $this->faker->freeEmail(),
        'phone'      => "{$this->faker->randomNumber(9, true)}"
    ];
});
