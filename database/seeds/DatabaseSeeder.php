<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        // Call production initial data
        $this->call([
            ProductionSeeder::class
        ]);

        // Call development initial data
        if(config('app.env') != 'production')
        {
            $this->call([
                DevelopmentSeeeder::class
            ]);
        }

    }
}
