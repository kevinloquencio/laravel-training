<?php

use Illuminate\Database\Seeder;
use App\User;

class ProductionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        // Create default administrator user
        factory(User::class)->state('role_admin')->create([
            'name'     => 'Administrator',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);
    }
}
