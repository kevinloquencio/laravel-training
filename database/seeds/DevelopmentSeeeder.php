<?php

use App\{Company, Employee, User};
use Illuminate\Database\Seeder;

class DevelopmentSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0;");

        Company::truncate();
        Employee::truncate();

        factory(User::class,2)->state('role_normal')->create();

        // Create dummy 
        factory(Company::class,100)->create()
        ->each(function($compay){
            
            factory(Employee::class,4)->create([
                'company_id' => $compay->id
            ]);

        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1;");
    }
}
