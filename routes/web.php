<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes( [
    'register' => false
] );

Route::get('/', 'HomeController');
Route::get('/home', 'HomeController')->name('home');

Route::resource('employees','EmployeeController')->middleware(['manage.employees','auth']);

Route::resource('companies','CompanyController')->middleware(['manage.companies','auth']);

Route::resource('accounts','AccountController')->middleware(['can:manage-app','auth']);


Route::prefix('accounts/{account}/change-password')->name('accounts')
    ->middleware(['can:manage-app','auth'])
    ->group(function(){

        Route::get('','AccountController@showChangePassword')->name('.change_password');
        Route::post('','AccountController@updatePassword')->name('.update_password');

    });